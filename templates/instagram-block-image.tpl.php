<?php
/**
 * @file
 * Default theme implementation of an Instagram image link.
 *
 * Available variables:
 * - post: The entire data array returned from the Instagram API request.
 * - href: The url to the Instagram post page.
 * - src: The source url to the instagram image.
 * - width: The display width of the image.
 * - height: The display height of the image.
 */
?>
<div class="instagram-item">
  <div class="instagram-user-picture"><img style="width:50px; height:50px" src="<?php print $post->user->profile_picture; ?>" /></div>
  <div class="instagram-date"><?php print $post->created; ?></div>
  <div class="instagram-user-name"><a href="<?php print $href; ?>"><?php print $post->user->full_name; ?></a></div>
  <div class="instagram-user-username"><?php print $post->user->username; ?></div>
<?php
$image_variables = array(
  'path' => $src,
  'width' => $width,
  'height' => $height,
);
?>
  <div class="instagram-image"><a href="<?php print $href; ?>"><?php print theme_image($image_variables); ?></a></div>
  <div class="instagram-caption"><?php print $post->caption->text; ?></div>
</div>
