(function ($, Drupal, window, document, undefined) {

  function setDefaultText(field, text) {
    var $field;

    $field = $(field);

    $field.val(text);

    $field.on('focusin', function () {
      if ($field.val() === text) {
        $field.val('');
      }
    });

    $field.on('focusout', function () {
      if ($field.val() === '') {
        $field.val(text);
      }
    });
  }

  Drupal.behaviors.sizeCalendar = {
    attach: function (context) {
      if (context != document) return;

      var cal = $(".calendar-calendar table.mini");
      var calHeight;

      calHeight = cal.width() * 0.7;
      cal.css('height', calHeight);

      $(window).resize(function() {
        calHeight = cal.width() * 0.7;
        cal.css('height', calHeight);
      });
    }
  }

  Drupal.behaviors.easydropdown = {
    attach: function () {
      var $selects = $('.events_calendar_filters select');

      $selects.easyDropDown({
        wrapperClass: 'easydropdown',
      });
    }
  }

  Drupal.behaviors.acalogContent = {
    attach: function() {
      var $acalog = $('.acalog');
      if ($acalog.length && $.fn.acalogWidgetize) {
        $acalog.acalogWidgetize({
          gateway: 'http://catalog.wwu.edu'
        });
      }
    }
  }

}) (jQuery, Drupal, this, this.document);
